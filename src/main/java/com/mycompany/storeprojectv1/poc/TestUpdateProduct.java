/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeprojectv1.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jarew
 */
public class TestUpdateProduct {

    public static void main(String[] args) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();

        try {
            String sql = "UPDATE product SET product_name = ?, product_price = ? WHERE product_id = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, "Americano2");
            stmt.setDouble(2, 45);
            stmt.setInt(3, 4);
            int row = stmt.executeUpdate();
            System.out.println("Affect row : " + row );

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
    }
}
