/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeprojectv1.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author jarew
 */
public class TestInsertProduct {

    public static void main(String[] args) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();

        try {
            String sql = "INSERT INTO product (product_name, product_price ) VALUES (  ?, ? );";
            PreparedStatement stmt = con.prepareStatement(sql);
            Product product = new Product(-1,"Green Tea",50);
            stmt.setString(1, product.getName());
            stmt.setDouble(2, product.getPrice());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                System.out.println("Affect row : " + row + "\nid :" + result.getInt(1));
            }

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
    }
}
