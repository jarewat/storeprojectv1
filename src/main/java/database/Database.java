/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;
//Singleton pattern

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jarew
 */
public class Database {

    private Connection con;
    private static Database instance = new Database();

    private Database() {

    }

    public static Database getInstance() {

        String dbPath = "./db/store.db";
        try {
            if (instance.con == null || !instance.con.isClosed()) {
                Class.forName("org.sqlite.JDBC");
                instance.con = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
                System.out.println("Database connected");
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Error : JDBC is not exist");
        } catch (SQLException ex) {
            System.out.println("Error : cannot connect Database");

        }
        return instance;
    }

    public static void close() {
        try {
            if (instance.con != null || !instance.con.isClosed()) {
                instance.con.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        instance.con = null;
    }

    public Connection getConnection() {
        return instance.con;
    }
}
