/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.storeprojectv1.poc.TestSelectProduct;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author jarew
 */
public class ProductDao implements DaoInterface<Product> {
    
    @Override
    public int add(Product object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO product (product_name, product_price ) VALUES (  ?, ? );";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Database.close();
        return id;
    }
    
    @Override
    public ArrayList<Product> getAll() {
        ArrayList<Product> list = new ArrayList<>();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        
        try {
            String sql = "SELECT product_id, product_name, product_price FROM product;";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("product_id");
                String name = result.getString("product_name");
                double price = result.getDouble("product_price");
                Product product = new Product(id, name, price);
                list.add(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Database.close();
        return list;
    }
    
    @Override
    public Product getById(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        
        try {
            String sql = "SELECT product_id, product_name, product_price FROM product WHERE product_id = " + id;
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("product_id");
                String name = result.getString("product_name");
                double price = result.getDouble("product_price");
                Product product = new Product(pid, name, price);
                Database.close();
                return product;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Database.close();
        return null;
    }
    
    @Override
    public int delete(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM product WHERE product_id = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Database.close();
        return row;
    }
    
    @Override
    public int update(Product object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE product SET product_name = ?, product_price = ? WHERE product_id = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Database.close();
        return row;
    }
    
    public static void main(String[] args) {
        ProductDao dao = new ProductDao();
        System.out.println(dao.getAll());
        System.out.println(dao.getById(1));
        int id = dao.add(new Product(-1, "Tea", 30));
        System.out.println("id : " + id);
        Product lastProduct = dao.getById(id);
        System.out.println("last"+lastProduct);
        lastProduct.setPrice(100);
        dao.update(lastProduct);
        Product updateProduct = dao.getById(id);
        System.out.println("update"+updateProduct);
        dao.delete(id);
    }
    
}
